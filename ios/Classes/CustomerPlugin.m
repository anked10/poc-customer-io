#import "CustomerPlugin.h"
#if __has_include(<customer/customer-Swift.h>)
#import <customer/customer-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "customer-Swift.h"
#endif

@implementation CustomerPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftCustomerPlugin registerWithRegistrar:registrar];
}
@end
