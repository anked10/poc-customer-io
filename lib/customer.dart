
import 'customer_platform_interface.dart';

class Customer {
  Future<String?> getPlatformVersion() {
    return CustomerPlatform.instance.getPlatformVersion();
  }
}
