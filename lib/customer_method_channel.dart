import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'customer_platform_interface.dart';

/// An implementation of [CustomerPlatform] that uses method channels.
class MethodChannelCustomer extends CustomerPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('customer');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
