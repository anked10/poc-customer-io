import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:customer/customer_method_channel.dart';

void main() {
  MethodChannelCustomer platform = MethodChannelCustomer();
  const MethodChannel channel = MethodChannel('customer');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await platform.getPlatformVersion(), '42');
  });
}
