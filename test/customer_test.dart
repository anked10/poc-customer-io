import 'package:flutter_test/flutter_test.dart';
import 'package:customer/customer.dart';
import 'package:customer/customer_platform_interface.dart';
import 'package:customer/customer_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockCustomerPlatform
    with MockPlatformInterfaceMixin
    implements CustomerPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final CustomerPlatform initialPlatform = CustomerPlatform.instance;

  test('$MethodChannelCustomer is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelCustomer>());
  });

  test('getPlatformVersion', () async {
    Customer customerPlugin = Customer();
    MockCustomerPlatform fakePlatform = MockCustomerPlatform();
    CustomerPlatform.instance = fakePlatform;

    expect(await customerPlugin.getPlatformVersion(), '42');
  });
}
