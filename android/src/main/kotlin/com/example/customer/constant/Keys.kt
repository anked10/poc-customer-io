package com.example.customer.constant

import io.customer.sdk.data.model.Region
import io.customer.sdk.util.CioLogLevel


internal object Keys {
    object Environment {
        const val SITE_ID = "siteId"
        const val API_KEY = "apiKey"
        val REGION = Region.US
    }

    object Config {
        const val TRACKING_API_URL = "trackingApiUrl"
        const val AUTO_TRACK_DEVICE_ATTRIBUTES = true
        val LOG_LEVEL = CioLogLevel.DEBUG
        const val AUTO_TRACK_PUSH_EVENTS = true
        const val BACKGROUND_QUEUE_MIN_NUMBER_OF_TASKS = 100
        const val BACKGROUND_QUEUE_SECONDS_DELAY = 8000.0
    }
}