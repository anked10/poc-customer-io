package com.example.customer

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import androidx.annotation.NonNull
import io.customer.sdk.CustomerIO
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import org.json.JSONObject
import org.json.JSONTokener

/** CustomerPlugin */
class CustomerPlugin: FlutterPlugin, MethodCallHandler, ActivityAware {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel
  private lateinit var activity: Activity;
  private lateinit var context: Context;

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    context = flutterPluginBinding.applicationContext
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "customer")
    channel.setMethodCallHandler(this)
  }

  @SuppressLint("WrongThread")
  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    CustomerIOEventsRepositoryImp().init(context)
    when (call.method) {
      "getPlatformVersion" -> {
        result.success("Android ${android.os.Build.VERSION.RELEASE}")
      }
      "initCustomerIO" -> {
        CustomerIOEventsRepositoryImp().init(context)
        result.success("OK")
      }

      "identify" -> {
        CustomerIO.instance().identify(call.arguments.toString())
        result.success("OK")
      }
      "track" -> {
        val json = call.arguments
        val jsonObject = JSONTokener(json.toString()).nextValue() as JSONObject
        CustomerIOEventsRepositoryImp().track(
          name = "Angelo",
          attributes = mapOf("NAME" to "name", "IS_GUEST" to "isGuest")
        )
        result.success("OK")
      }
      "registerDeviceToken" -> {
        val json = call.arguments
        val jsonObject = JSONTokener(json.toString()).nextValue() as JSONObject
        CustomerIOEventsRepositoryImp().registerDeviceToken(deviceToken = "Angelo")
        result.success("OK")
      }
      "clearIdentify" -> {
        CustomerIOEventsRepositoryImp().clearIdentify()
        result.success("OK")
      }

      "fcmNewToken" -> {
        val json = call.arguments
        val jsonObject = JSONTokener(json.toString()).nextValue() as JSONObject
        CustomerIOEventsRepositoryImp().fcmNewToken(token = "Angelo")
        result.success("OK")
      }

      "screen" -> {
        val json = call.arguments
        val jsonObject = JSONTokener(json.toString()).nextValue() as JSONObject
        CustomerIOEventsRepositoryImp().screen(
          name = "Angelo",
          attributes = mapOf("NAME" to "name", "IS_GUEST" to "isGuest")
        )
        result.success("OK")
      }
      else -> {
        result.notImplemented()
      }
    }
  }




  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

  override fun onAttachedToActivity(binding: ActivityPluginBinding) {
    TODO("Not yet implemented")
    activity = binding.activity
  }

  override fun onDetachedFromActivityForConfigChanges() {
    TODO("Not yet implemented")
  }

  override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
    TODO("Not yet implemented")
  }

  override fun onDetachedFromActivity() {
    TODO("Not yet implemented")
  }
}
