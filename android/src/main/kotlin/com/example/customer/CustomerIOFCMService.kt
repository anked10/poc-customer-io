package com.example.customer

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import io.customer.messagingpush.CustomerIOFirebaseMessagingService

class CustomerIOReactNativeFCMService : FirebaseMessagingService() {
    override fun onMessageReceived(message: RemoteMessage) {
        CustomerIOEventsRepositoryImp().init(applicationContext)
        CustomerIOFirebaseMessagingService.onMessageReceived(message)
    }

    override fun onNewToken(token: String) {
        CustomerIOFirebaseMessagingService.onNewToken(token)
    }
}