package com.example.customer

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.annotation.WorkerThread
import com.example.customer.constant.Keys
import com.google.firebase.messaging.RemoteMessage
import io.customer.messagingpush.CustomerIOFirebaseMessagingService
import io.customer.messagingpush.MessagingPushModuleConfig
import io.customer.messagingpush.ModuleMessagingPushFCM
import io.customer.sdk.CustomerIO
import io.customer.sdk.util.CioLogLevel

interface EventsRepository {
    fun identify(identifier: String, attributes: Map<String, Any>)
    fun track(name: String, attributes: Map<String, Any> = emptyMap())
    fun registerDeviceToken(deviceToken: String)
    fun clearIdentify()
    fun init(context: Context)
    fun screen(name: String, attributes: Map<String, Any> = emptyMap())
    fun fcmNewToken(token: String)
    fun fcmMessageReceived(message: RemoteMessage)
}

 class CustomerIOEventsRepositoryImp() : EventsRepository {

    @WorkerThread
    override fun identify(identifier: String, attributes: Map<String, Any>) {
        CustomerIO.instance().identify(identifier = identifier, attributes = attributes)
    }

    override fun track(name: String, attributes: Map<String, Any>) {
        CustomerIO.instance().track(name = name, attributes = attributes)
    }

    override fun registerDeviceToken(deviceToken: String) {
        CustomerIO.instance().registerDeviceToken(deviceToken = deviceToken)
    }

     override fun clearIdentify() {
        CustomerIO.instance().clearIdentify()
    }

     override fun init(context: Context) {
         try {
             if (CustomerIO.instanceOrNull() != null) return
             val builder = CustomerIO.Builder(
                 siteId = Keys.Environment.SITE_ID,
                 apiKey =  Keys.Environment.API_KEY,
                 region = Keys.Environment.REGION,
                 appContext = context as Application
             )
             builder.setTrackingApiURL(Keys.Config.TRACKING_API_URL)
             builder.autoTrackDeviceAttributes(Keys.Config.AUTO_TRACK_DEVICE_ATTRIBUTES)
             builder.autoTrackDeviceAttributes(true)
             builder.setLogLevel(Keys.Config.LOG_LEVEL);
             builder.setBackgroundQueueSecondsDelay(Keys.Config.BACKGROUND_QUEUE_SECONDS_DELAY)
             builder.setBackgroundQueueMinNumberOfTasks(Keys.Config.BACKGROUND_QUEUE_MIN_NUMBER_OF_TASKS)
             builder.addCustomerIOModule(
                 ModuleMessagingPushFCM(
                     config = MessagingPushModuleConfig.Builder().apply {
                         setAutoTrackPushEvents(Keys.Config.AUTO_TRACK_PUSH_EVENTS)
                         setRedirectDeepLinksToOtherApps(false)
                     }.build()
                 )
             )
             Log.i("","Customer.io instance initialized successfully from preferences")
         } catch (ex: Exception) {
             Log.e("","Failed to initialize Customer.io instance from preferences, ${ex.message}")
         }
     }

     override fun screen(name: String, attributes: Map<String, Any>) {
        CustomerIO.instance().screen(name = name, attributes = attributes)
    }

    //Eventos for FCM
     override fun fcmNewToken(token: String) {
         CustomerIOFirebaseMessagingService.onNewToken(token)
     }
     override fun fcmMessageReceived(message: RemoteMessage) {
         val handled = CustomerIOFirebaseMessagingService.onMessageReceived(message)
     }

 }
